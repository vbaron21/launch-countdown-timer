 // Definir la fecha y hora del lanzamiento (Año, Mes (0-11), Día, Hora, Minuto, Segundo)
  const launchDate = new Date(2024, 6, 1, 12, 0, 0).getTime(); // Fecha y hora del lanzamiento (1 de julio de 2024, 12:00 PM)

  // Actualizar el temporizador cada segundo
  const countdownTimer = setInterval(() => {
    // Obtener la fecha y hora actual
    const now = new Date().getTime();
    
    // Calcular la diferencia entre la fecha y hora del lanzamiento y la fecha y hora actual
    const difference = launchDate - now;

    // Calcular días, horas, minutos y segundos restantes
    const days = Math.floor(difference / (1000 * 60 * 60 * 24));
    const hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((difference % (1000 * 60)) / 1000);

    // Obtener los valores de los campos del formulario
    const secondsCounter = document.getElementById("secondsCounter");
    const minutesCounter = document.getElementById("minutesCounter");
    const hoursCounter = document.getElementById("hoursCounter");
    const daysCounter = document.getElementById("daysCounter");
  
    secondsCounter.innerHTML = `${seconds}`;
    minutesCounter.innerHTML = `${minutes}`;
    hoursCounter.innerHTML = `${hours}`;
    daysCounter.innerHTML = `${days}`;

    // Si el temporizador llega a cero, detener el temporizador
    if (difference < 0) {
      clearInterval(countdownTimer);
      document.getElementById("countdown").innerHTML = "¡Lanzamiento!";
    }
  }, 1000); // Intervalo de actualización (cada segundo)